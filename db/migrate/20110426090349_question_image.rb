class QuestionImage < ActiveRecord::Migration
  def self.up
      add_column :questionbanks, :question_image_file_name, :string

  end

  def self.down
    remove_column :questionbanks, :question_image_file_name
  end
end
