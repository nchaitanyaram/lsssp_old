class UserexaminationsController < ApplicationController
  layout "admin"
  def index
    @userexaminations = Userexamination.all

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @userexaminations }
    end
  end

  def show
    @userexamination = Userexamination.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @userexamination }
    end
  end

  def new
    @userexamination = Userexamination.new
	@examinations = Examination.all
  	@user_id = params[:user_id]
  	@user_name = User.find(params[:user_id]).email

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @userexamination }
    end
  end

  def edit
    @userexamination = Userexamination.find(params[:id])
  end

  def create
    @userexamination = Userexamination.new(params[:userexamination])
    @examinations = Examination.all
     
    respond_to do |format|
      if @userexamination.save
        format.html { redirect_to(@userexamination, :notice => 'User Examination was successfully created.') }
        format.xml  { render :xml => @userexamination, :status => :created, :location => @userexamination }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @userexamination.errors, :status => :unprocessable_entity }
      end
    end
  end

  def update
    @userexamination = Userexamination.find(params[:id])

    respond_to do |format|
      if @userexamination.update_attributes(params[:userexamination])
        format.html { redirect_to(@userexamination, :notice => 'Userexamination was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @userexamination.errors, :status => :unprocessable_entity }
      end
    end
  end

  def destroy
    @userexamination = Userexamination.find(params[:id])
    @userexamination.destroy

    respond_to do |format|
      format.html { redirect_to(userexaminations_url) }
      format.xml  { head :ok }
    end
  end
end
