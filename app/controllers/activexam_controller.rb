class ActivexamController < ApplicationController
 layout "exam"
 before_filter :require_user, :only => [:index,:show, :edit, :update, :create]
	
 def index
# 	@questions = Questionbank.all
#  	@examinations = Examination.find(params[:exam_id])
 end

 def new
#   @activexam = Activexam.new
 end
 
 def create
	@exam_active = Userexamination.find(:first, :conditions=>{ :user_id=>@created_by, :examination_id=>params[:exam_id]})

    #check if user is completed exam or not 
    if @exam_active.exam_complete_status == true
        redirect_to :controller=> 'activetests', :action=>'testcompleted', :exam_id => params[:exam_id]
   else # for exam completed or not
		@exam_active.exam_active_status =1
		@exam_active.number_of_attempts = @exam_active.number_of_attempts + 1
		@exam_active.save

		@user_exam_init = Activexam.find(:all, :conditions=>{:user_id => @created_by, :examination_id=>params[:exam_id]})
		
	 	@examination = Examination.find(params[:exam_id])
		module_questions = Examination.find(params[:exam_id]).module_questions.split(",")
		@module_id_array = Array.new
		@question_id_array = Array.new
		@no_of_q = 0
		module_questions.each do |module_questions|
			m_q_array = module_questions.split(":")
	 	    @module_id_array << m_q_array[0]
	# 	    puts "##########"
	# 	    puts @module_id_array.size
	 	    @question_id_array << m_q_array[1]   	
	 	    @no_of_q = @no_of_q + m_q_array[1].to_i
				    
		end	
        if @user_exam_init.size >= 1 
#		 puts "##########Chaitanya###############"
			@activexams = Activexam.find(:all, :conditions => {:examination_id => params[:exam_id], :user_id => @created_by, :subject_id=>@module_id_array})

		else
		(0..@module_id_array.size-1).each do | m|
				(0..@question_id_array[m].to_i-1).each do |q |
					@activexam = Activexam.new(:user_id => @created_by,:examination_id => params[:exam_id],:subject_id => @module_id_array[m] , :question_id => q+1)
	 			    @activexam.save
				end
		end
			@activexams = Activexam.find(:all, :conditions => {:examination_id => params[:exam_id], :user_id => @created_by, :subject_id=>@module_id_array})
		end #exam_int	 
 	end # End of If condition Exam completed or not Check
	
 end
 
 def show
 	 @question = Questionbank.find(params[:id])

    respond_to do |format|
	   format.js
    end
  end 
  
  def test_complete
#  	exam_id = params[:exam_id]
  	
  	@total_notanswer = Activexam.count(:conditions=>{:answer => nil, :user_id => @created_by, :examination_id =>params[:exam_id]})
  	@total_questions = Activexam.count(:conditions => {:examination_id => params[:exam_id], :user_id=>@created_by})
	@total_answered = @total_questions - @total_notanswer
 
	@exam_active = Userexamination.find(:first, :conditions=>{ :user_id=>@created_by, :examination_id=>params[:exam_id]})
    @exam_active.exam_complete_status =1
    @exam_active.questions_answered = @total_answered
    @exam_active.no_of_questions = @total_questions
	@exam_active.save
 	
  end

  def answer
	qid =  params[:question_id]
	answer =  params[:answer]
	sid = params[:subject_id]
#    tid = params[:time]
##	puts "Answer Called"
	@exam = Activexam.find(:first, :conditions=>{ :user_id=>@created_by, :subject_id=> sid, :question_id => qid })
	@exam.answer = answer
	@exam.save
  end
  
  
end
