class UserMailer < ActionMailer::Base
 default :from => "lsssp@lsssp.com"
 
  def welcome_email(user)
    @user = user
    @url  = "http://123.176.41.122:3000"
    mail(:to => user.email,
         :subject => "Welcome to My Awesome Site")
  end
end
